import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:myapp/learn_more/view/createLearMore.dart';

var connectivity = true;
FirebaseFirestore firestore = FirebaseFirestore.instance;

class LearMoreHome extends StatefulWidget {
  @override
  _LearMoreHomeState createState() => _LearMoreHomeState();
}

Future<bool> check() async {
  var connectivityResult = await (Connectivity().checkConnectivity());

  if (connectivityResult == ConnectivityResult.mobile) {
    connectivity = true;

    return true;
  } else if (connectivityResult == ConnectivityResult.wifi) {
    connectivity = true;

    return true;
  }
  connectivity = false;

  return false;
}

class BlogsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CollectionReference fire = firestore.collection('LearnMore');

    return StreamBuilder<QuerySnapshot>(
      stream: fire.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Container(
            alignment: Alignment.center,
            child: CircularProgressIndicator(),
          );
        }
        return new ListView(
          padding: EdgeInsets.symmetric(horizontal: 16),
          children: snapshot.data.docs.map((DocumentSnapshot document) {
            return BlogsTyle(
              dsc: document.data()['desc'],
              title: document.data()['title'],
              img: document.data()['image'],
              autor: document.data()['autorName'],
            );
          }).toList(),
        );
      },
    );
  }
}

class _LearMoreHomeState extends State<LearMoreHome> {
  CollectionReference fire = FirebaseFirestore.instance.collection('LearnMore');

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(children: <Widget>[
            Text(
              "Learn More",
              style: TextStyle(fontSize: 22),
            ),
            Text(
              "Learn More",
              style: TextStyle(fontSize: 22, color: Colors.blue),
            )
          ]),
        ),
        body: BlogsList(),
        floatingActionButton: Container(
          padding: EdgeInsets.symmetric(vertical: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FloatingActionButton(
                onPressed: () {
                  check();
                  Future.delayed(const Duration(milliseconds: 500), () {
                    if (connectivity) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CreateLearMore()));
                    } else {
                      return showDialog(
                          context: context,
                          builder: (context) {
                            return SimpleDialog(
                              title: Text("Connectivity Needed"),
                              children: <Widget>[
                                Center(
                                    child: Text(
                                        "You cannot create a new blog because "
                                        "the connection is lost. Please review and try again")),
                                Center(
                                    child: TextButton(
                                        child: Text('OK'),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        }))
                              ],
                            );
                          });
                    }
                  });
                },
                child: Icon(Icons.add),
              )
            ],
          ),
        ));
  }
}

class BlogsTyle extends StatelessWidget {
  String img, title, dsc, autor;
  BlogsTyle(
      {@required this.img,
      @required this.title,
      @required this.autor,
      @required this.dsc});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      height: 150,
      child: Stack(children: <Widget>[
        ClipRRect(
            borderRadius: BorderRadius.circular(6),
            child: CachedNetworkImage(
                imageUrl: img,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.cover)),
        Container(
          height: 150,
          decoration: BoxDecoration(
              color: Colors.black45.withOpacity(0.3),
              borderRadius: BorderRadius.circular(6)),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 22,
                      fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: 4,
                ),
                Text(dsc,
                    style: TextStyle(
                      color: Colors.white,
                    )),
                SizedBox(
                  height: 4,
                ),
                Text(autor,
                    style: TextStyle(
                      color: Colors.white,
                    ))
              ]),
        )
      ]),
    );
  }
}
