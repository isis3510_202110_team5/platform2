import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:myapp/learn_more/view/learMoreHome.dart';

class CreateLearMore extends StatefulWidget {
  @override
  _CreateLearMoreState createState() => _CreateLearMoreState();
}

var connectivity = true;

Future<bool> check() async {
  var connectivityResult = await (Connectivity().checkConnectivity());

  if (connectivityResult == ConnectivityResult.mobile) {
    connectivity = true;

    return true;
  } else if (connectivityResult == ConnectivityResult.wifi) {
    connectivity = true;

    return true;
  }
  connectivity = false;

  return false;
}

class _CreateLearMoreState extends State<CreateLearMore> {
  String autorName, title, desc, urlImagen;
  File selectedImage;
  int _isLoggin = 0;
  Future getImage() async {
    final image = await ImagePicker.pickImage(source: ImageSource.camera);
    if (image == null) {
      return showDialog(
          context: context,
          builder: (context) {
            return SimpleDialog(
              title: Text("Valid Image"),
              children: <Widget>[
                Center(
                    child:
                        Text("Please verify that the image is from your gallery"
                            "and is not, for example, a WhatsApp image.")),
                Center(
                    child: TextButton(
                        child: Text('OK'),
                        onPressed: () {
                          Navigator.pop(context);
                        }))
              ],
            );
          });
    }

    setState(() {
      selectedImage = image;
    });
  }

  Future<void> retrieveLostData() async {
    final LostDataResponse response = await ImagePicker.retrieveLostData();
    if (response == null) {
      return;
    }
    if (response.file != null) {
      setState(() {
        _handleImage(response.file);
      });
    } else {
      _handleError(response.exception);
    }
  }

  _handleImage(File file) {
    setState(() {
      selectedImage = file;
    });
  }

  _handleError(PlatformException exception) {
    print(exception);
  }

  // ignore: missing_return
  Future<void> uploadBlog() {
    if (urlImagen != null &&
        autorName != null &&
        title != null &&
        desc != null) {
      setState(() {
        _isLoggin = 1;
      });
      //agregar imagen a la base de datos
      CollectionReference fireStore =
          FirebaseFirestore.instance.collection('LearnMore');
      return fireStore
          .add({
            'image': urlImagen, // John Doe
            'autorName': autorName, // Stokes and Sons
            'title': title,
            'desc': desc
          })
          .then((value) => showDialog(
              context: context,
              builder: (context) {
                return SimpleDialog(
                  title: Text("OK"),
                  children: <Widget>[
                    Center(child: Text("The information was added")),
                    Center(
                        child: TextButton(
                            child: Text('OK'),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LearMoreHome()));
                            }))
                  ],
                );
              }))
          .catchError((error) => print("Failed to add user: $error"));
    } else {
      return showDialog(
          context: context,
          builder: (context) {
            return SimpleDialog(
              title: Text("Error"),
              children: <Widget>[
                Center(child: Text("Please fill in all the data")),
                Center(
                    child: TextButton(
                        child: Text('OK'),
                        onPressed: () {
                          Navigator.pop(context);
                        }))
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(children: <Widget>[
          Text(
            "Learn More:",
            style: TextStyle(fontSize: 22),
          ),
          Text(
            "Create",
            style: TextStyle(fontSize: 20, color: Colors.black),
          )
        ]),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              uploadBlog();
              retrieveLostData();
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Icon(Icons.update),
            ),
          ),
        ],
      ),
      body: _isLoggin == 1
          ? Container(
              alignment: Alignment.center,
              child: CircularProgressIndicator(),
            )
          : Container(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  GestureDetector(
                      onTap: () {
                        check();
                        Future.delayed(const Duration(milliseconds: 500), () {
                          if (connectivity) {
                            getImage();
                          } else {
                            return showDialog(
                                context: context,
                                builder: (context) {
                                  return SimpleDialog(
                                    title: Text("Connectivity Needed"),
                                    children: <Widget>[
                                      Center(
                                          child: Text(
                                              "You cannot create a new blog "
                                              "the connection is lost. Please review and try again")),
                                      Center(
                                          child: TextButton(
                                              child: Text('OK'),
                                              onPressed: () {
                                                Navigator.pop(context);
                                              }))
                                    ],
                                  );
                                });
                          }
                        });
                      },
                      child: selectedImage != null
                          ? Container(
                              margin: EdgeInsets.symmetric(horizontal: 16),
                              height: 150,
                              width: MediaQuery.of(context).size.width,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(6),
                                child: Image.file(selectedImage,
                                    fit: BoxFit.cover),
                              ),
                            )
                          : Container(
                              margin: EdgeInsets.symmetric(horizontal: 16),
                              height: 150,
                              decoration: BoxDecoration(
                                  color: Colors.black12,
                                  borderRadius: BorderRadius.circular(6)),
                              width: MediaQuery.of(context).size.width,
                              child: Icon(
                                Icons.add_a_photo,
                                color: Colors.cyan,
                              ),
                            )),
                  SizedBox(height: 8),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 16),
                    child: Column(children: <Widget>[
                      TextField(
                        decoration: InputDecoration(hintText: "Autor Name"),
                        onChanged: (val) {
                          autorName = val;
                        },
                      ),
                      TextField(
                        decoration: InputDecoration(hintText: "Title"),
                        onChanged: (val) {
                          title = val;
                        },
                      ),
                      TextField(
                        decoration: InputDecoration(hintText: "Description"),
                        onChanged: (val) {
                          desc = val;
                        },
                      ),
                      TextField(
                        decoration: InputDecoration(hintText: "Image Url"),
                        onChanged: (val) {
                          urlImagen = val;
                        },
                      )
                    ]),
                  ),
                ],
              ),
            ),
    );
  }
}
