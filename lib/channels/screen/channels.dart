import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:myapp/channels/view/createChannel.dart';
import 'ChannelComents.dart';

String actualId = "KVZsJ7QmDRg6FiWPRyTr";
var connectivity = true;
FirebaseFirestore firestore = FirebaseFirestore.instance;

class Channels extends StatefulWidget {
  @override
  _ChannelsState createState() => _ChannelsState();
}

class BlogsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CollectionReference fire = firestore.collection('Channel2');

    return StreamBuilder<QuerySnapshot>(
      stream: fire.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Container(
            alignment: Alignment.center,
            child: CircularProgressIndicator(),
          );
        }
        return new ListView(
          padding: EdgeInsets.symmetric(horizontal: 16),
          children: snapshot.data.docs.map((DocumentSnapshot document) {
            return BlogsTyle(
                dsc: document.data()['desc'],
                title: document.data()['title'],
                img: document.data()['image'],
                subTitle: document.data()['subTitle'],
                id: document.id);
          }).toList(),
        );
      },
    );
  }
}

Future<bool> check() async {
  var connectivityResult = await (Connectivity().checkConnectivity());

  if (connectivityResult == ConnectivityResult.mobile) {
    connectivity = true;

    return true;
  } else if (connectivityResult == ConnectivityResult.wifi) {
    connectivity = true;

    return true;
  }
  connectivity = false;

  return false;
}

class _ChannelsState extends State<Channels> {
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Channels'),
        ),
        body: BlogsList(),
        floatingActionButton: Container(
          padding: EdgeInsets.symmetric(vertical: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FloatingActionButton(
                onPressed: () {
                  check();
                  Future.delayed(const Duration(milliseconds: 500), () {
                    if (connectivity) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CreateChannel()));
                    } else {
                      return showDialog(
                          context: context,
                          builder: (context) {
                            return SimpleDialog(
                              title: Text("Connectivity Needed"),
                              children: <Widget>[
                                Center(
                                    child: Text(
                                        "You cannot create a new blog because "
                                        "the connection is lost. Please review and try again")),
                                Center(
                                    child: TextButton(
                                        child: Text('OK'),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        }))
                              ],
                            );
                          });
                    }
                  });
                },
                child: Icon(Icons.add),
              )
            ],
          ),
        ));
  }
}

class BlogsTyle extends StatelessWidget {
  String title, dsc, img, subTitle, id;
  BlogsTyle(
      {@required this.img,
      @required this.title,
      @required this.subTitle,
      @required this.dsc,
      @required this.id});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      height: 420,
      child: Stack(children: <Widget>[
        Card(
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: [
              ListTile(
                leading: Icon(Icons.arrow_drop_down_circle),
                title: Text(title),
                subtitle: Text(
                  subTitle,
                  style: TextStyle(color: Colors.black.withOpacity(0.6)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  dsc,
                  style: TextStyle(color: Colors.black.withOpacity(0.6)),
                ),
              ),
              CachedNetworkImage(imageUrl: img),
              ButtonBar(
                alignment: MainAxisAlignment.start,
                children: [
                  FlatButton(
                    onPressed: () {
                      check();
                      Future.delayed(const Duration(milliseconds: 500), () {
                        if (connectivity) {
                          actualId = id;
                          Navigator.push(context,
                              MaterialPageRoute(builder: (_) => TestMe()));
                        } else {
                          return showDialog(
                              context: context,
                              builder: (context) {
                                return SimpleDialog(
                                  title: Text("Connectivity Needed"),
                                  children: <Widget>[
                                    Center(
                                        child: Text(
                                            "The Channels could not be loaded because "
                                            "the connection is lost. Please review and try again")),
                                    Center(
                                        child: TextButton(
                                            child: Text('OK'),
                                            onPressed: () {
                                              Navigator.pop(context);
                                            }))
                                  ],
                                );
                              });
                        }
                      });
                    },
                    child: const Text('See more'),
                  ),
                ],
              ),
            ],
          ),
        )
      ]),
    );
  }
}

ThemeData _buildShrineTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    buttonTheme: const ButtonThemeData(
      textTheme: ButtonTextTheme.normal,
    ),
    textTheme: _buildShrineTextTheme(base.textTheme),
    primaryTextTheme: _buildShrineTextTheme(base.primaryTextTheme),
    accentTextTheme: _buildShrineTextTheme(base.accentTextTheme),
  );
}

TextTheme _buildShrineTextTheme(TextTheme base) {
  return base
      .copyWith(
        caption: base.caption.copyWith(
          fontWeight: FontWeight.w400,
          fontSize: 14,
        ),
        button: base.button.copyWith(
          fontWeight: FontWeight.w500,
          fontSize: 14,
        ),
      )
      .apply(
        fontFamily: 'Rubik',
      );
}
