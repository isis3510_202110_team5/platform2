import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'Channels.dart' as globals;

String id = globals.actualId;
final FlutterTts flutterTts = FlutterTts();
var connectivity = true;
String name, pic, message;
FirebaseFirestore firestore = FirebaseFirestore.instance;

class TestMe extends StatefulWidget {
  @override
  _TestMeState createState() => _TestMeState();
}

Future<bool> check() async {
  var connectivityResult = await (Connectivity().checkConnectivity());

  if (connectivityResult == ConnectivityResult.mobile) {
    connectivity = true;

    return true;
  } else if (connectivityResult == ConnectivityResult.wifi) {
    connectivity = true;

    return true;
  }
  connectivity = false;

  return false;
}

class GetElement extends StatelessWidget {
  String documentId = id;
  @override
  Widget build(BuildContext context) {
    CollectionReference channels =
        FirebaseFirestore.instance.collection('Channel2');

    return FutureBuilder<DocumentSnapshot>(
      future: channels.doc(documentId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.hasData && !snapshot.data.exists) {
          return Text("Document does not exist");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data();
          return BlogsTyle(
            dsc: data['desc'],
            title: data['title'],
            img: data['image'],
            subTitle: data['subTitle'],
          );
        }

        return Text("loading");
      },
    );
  }
}

class GetComments extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CollectionReference fire = firestore.collection('messages2');

    return StreamBuilder<QuerySnapshot>(
      stream: fire.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Container(
            alignment: Alignment.center,
            child: CircularProgressIndicator(),
          );
        }
        return new Container(
            height: 500,
            child: ListView(
              padding: const EdgeInsets.all(8),
              children: snapshot.data.docs.map((DocumentSnapshot document) {
                if (document.data()['channel'] == id) {
                  return Mensagess(
                    name: document.data()['name'],
                    pic: document.data()['pic'],
                    message: document.data()['message'],
                  );
                }
              }).toList(),
            ));
      },
    );
  }
}

class _TestMeState extends State<TestMe> {
  Future<void> addComment() {
    if (pic != null && name != null && message != null) {
      CollectionReference fireStore =
          FirebaseFirestore.instance.collection('messages2');
      return fireStore
          .add({'name': name, 'pic': pic, 'message': message, 'channel': id})
          .then((value) => showDialog(
              context: context,
              builder: (context) {
                return SimpleDialog(
                  title: Text("OK"),
                  children: <Widget>[
                    Center(child: Text("The information was added")),
                    Center(
                        child: TextButton(
                            child: Text('OK'),
                            onPressed: () {
                              Navigator.pop(context);
                            }))
                  ],
                );
              }))
          .catchError((error) => print("Failed to add user: $error"));
    } else {
      return showDialog(
          context: context,
          builder: (context) {
            return SimpleDialog(
              title: Text("Error"),
              children: <Widget>[
                Center(child: Text("Please fill in all the data")),
                Center(
                    child: TextButton(
                        child: Text('OK'),
                        onPressed: () {
                          Navigator.pop(context);
                        }))
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: [
      Container(
        child: GetElement(),
      ),
      Text(" Comments", style: TextStyle(fontWeight: FontWeight.bold)),
      Container(
          child: Padding(
              padding: const EdgeInsets.fromLTRB(2.0, 8.0, 2.0, 0.0),
              child: Column(children: <Widget>[
                ListTile(
                  leading: GestureDetector(
                      child: Container(
                    height: 50.0,
                    width: 50.0,
                    decoration: new BoxDecoration(
                        color: Colors.blue,
                        borderRadius:
                            new BorderRadius.all(Radius.circular(50))),
                    child: CircleAvatar(
                        radius: 50,
                        backgroundImage: NetworkImage(
                            'https://lh3.googleusercontent.com/a-/AOh14GjRHcaendrf6gU5fPIVd8GIl1OgblrMMvGUoCBj4g=s400')),
                  )),
                ),
                TextField(
                    decoration: InputDecoration(hintText: "Write a comment..."),
                    onChanged: (val) {
                      message = val;
                    }),
                GestureDetector(
                  onTap: () {
                    check();
                    Future.delayed(const Duration(milliseconds: 500), () {
                      if (connectivity) {
                        pic =
                            "https://lh3.googleusercontent.com/a-/AOh14GjRHcaendrf6gU5fPIVd8GIl1OgblrMMvGUoCBj4g=s400";
                        name = "New User";
                        addComment();
                      } else {
                        return showDialog(
                            context: context,
                            builder: (context) {
                              return SimpleDialog(
                                title: Text("Connectivity Needed"),
                                children: <Widget>[
                                  Center(
                                      child: Text(
                                          "You cannot create a new commentary "
                                          "the connection is lost. Please review and try again")),
                                  Center(
                                      child: TextButton(
                                          child: Text('OK'),
                                          onPressed: () {
                                            Navigator.pop(context);
                                          }))
                                ],
                              );
                            });
                      }
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Icon(Icons.add_comment),
                  ),
                ),
              ]))),
      Container(
        child: GetComments(),
      ),
    ]));
  }
}

class BlogsTyle extends StatelessWidget {
  String title, dsc, img, subTitle, id;
  BlogsTyle(
      {@required this.img,
      @required this.title,
      @required this.subTitle,
      @required this.dsc});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      height: 420,
      child: Stack(children: <Widget>[
        Card(
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: [
              ListTile(
                leading: Icon(Icons.arrow_drop_down_circle),
                title: Text(title),
                subtitle: Text(
                  subTitle,
                  style: TextStyle(color: Colors.black.withOpacity(0.6)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  dsc,
                  style: TextStyle(color: Colors.black.withOpacity(0.6)),
                ),
              ),
              CachedNetworkImage(imageUrl: img),
            ],
          ),
        )
      ]),
    );
  }
}

class Mensagess extends StatelessWidget {
  String name, pic, message;
  Mensagess({@required this.name, @required this.message, @required this.pic});
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Padding(
          padding: const EdgeInsets.fromLTRB(2.0, 8.0, 2.0, 0.0),
          child: ListTile(
              leading: GestureDetector(
                  child: Container(
                height: 50.0,
                width: 50.0,
                decoration: new BoxDecoration(
                    color: Colors.blue,
                    borderRadius: new BorderRadius.all(Radius.circular(50))),
                child: CircleAvatar(
                    radius: 50, backgroundImage: NetworkImage(pic)),
              )),
              title: Text(
                name,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Text(message))),
      RaisedButton(
        child: Text("Press to hear the comment"),
        onPressed: () => speak(),
      )
    ]);
  }

  speak() async {
    await flutterTts.speak(message);
  }
}
