import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

FirebaseFirestore firestore = FirebaseFirestore.instance;

class AddMessage extends StatelessWidget {
  final String channel;
  final String text;
  final String timestamp;
  final String user;

  AddMessage(this.channel, this.text, this.timestamp, this.user);

  @override
  Widget build(BuildContext context) {
    // Create a CollectionReference called messages that references the firestore collection
    CollectionReference messages =
        FirebaseFirestore.instance.collection('Message');

    Future<void> addMessage() {
      // Call the message's CollectionReference to add a new message
      return messages
          .add({
            'channel': channel,
            'text': text,
            'timestamp': timestamp,
            'user': user,
          })
          .then((value) => print("Message Added"))
          .catchError((error) => print("Failed to add message: $error"));
    }

    return TextButton(
      onPressed: addMessage,
      child: Text(
        "Add Message",
      ),
    );
  }
}

class GetMessageName extends StatelessWidget {
  final String documentId;

  GetMessageName(this.documentId);

  @override
  Widget build(BuildContext context) {
    CollectionReference messages =
        FirebaseFirestore.instance.collection('Message');

    return FutureBuilder<DocumentSnapshot>(
      future: messages.doc(documentId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.hasData && !snapshot.data.exists) {
          return Text("Document does not exist");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data();
          return Text("Full Name: ${data['text']} ${data['timestamp']}");
        }

        return Text("loading");
      },
    );
  }
}

// Widget to test the connection to db
class MessageInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CollectionReference messages = firestore.collection('Message');

    return StreamBuilder<QuerySnapshot>(
      stream: messages.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        return new ListView(
          children: snapshot.data.docs.map((DocumentSnapshot document) {
            return new ListTile(
              title: new Text(document.data()['text']),
              subtitle: new Text(document.data()['timestamp'].toString()),
            );
          }).toList(),
        );
      },
    );
  }
}

/// {@template message}
/// Message model
///
/// [Message.empty] represents an unauthenticated message.
/// {@endtemplate}
class Message extends Equatable {
  /// {@macro message}
  const Message({
    @required this.channel,
    @required this.text,
    @required this.timestamp,
    @required this.user,
  }) : assert(text != null);

  /// The current message's email address.
  final String channel;

  /// The current message's id.
  final String text;

  /// The current message's name (display name).
  final String timestamp;

  final String user;

  /// Empty message which represents an unauthenticated message.
  static const empty = Message(channel: '', text: '', timestamp: '', user: '');

  @override
  List<Object> get props => [channel, text, timestamp, user];
}
