import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:google_maps_flutter/google_maps_flutter.dart';

List<DojoDetails> parseDojos(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<DojoDetails>((json) => DojoDetails.fromJson(json)).toList();
}

class DataAccessObject {

  Future<List<DojoDetails>> fetchLocalDojos() async {
    String rawData = await rootBundle.loadString('assets/mocks/dojos.json');
    final parsed = jsonDecode(rawData)["dojos"].cast<Map<String, dynamic>>();
    return parsed.map<DojoDetails>((json) => DojoDetails.fromJson(json)).toList();
  }
}

class DojoDetails {

  final String name;
  final LatLng location;

  DojoDetails({@required this.name, @required this.location});

  factory DojoDetails.fromJson(Map<String, dynamic> json) {
    return DojoDetails(
      name: json['name'] as String,
      location: LatLng(json['location'][0], json['location'][1]),
    );
  }
}