import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

FirebaseFirestore firestore = FirebaseFirestore.instance;

class AddDojo extends StatelessWidget {
  final List<String> categories;
  final List<int> location;
  final String name;

  AddDojo(this.categories, this.location, this.name);

  @override
  Widget build(BuildContext context) {
    // Create a CollectionReference called dojos that references the firestore collection
    CollectionReference dojos = FirebaseFirestore.instance.collection('Dojo');

    Future<void> addDojo() {
      // Call the dojo's CollectionReference to add a new dojo
      return dojos
          .add({
            'categories': categories, // John Doe
            'location': location, // Stokes and Sons
            'name': name, // Stokes and Sons
          })
          .then((value) => print("Dojo Added"))
          .catchError((error) => print("Failed to add dojo: $error"));
    }

    return TextButton(
      onPressed: addDojo,
      child: Text(
        "Add Dojo",
      ),
    );
  }
}

class GetDojoName extends StatelessWidget {
  final String documentId;

  GetDojoName(this.documentId);

  @override
  Widget build(BuildContext context) {
    CollectionReference dojos = FirebaseFirestore.instance.collection('Dojo');

    return FutureBuilder<DocumentSnapshot>(
      future: dojos.doc(documentId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.hasData && !snapshot.data.exists) {
          return Text("Document does not exist");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data();
          return Text(
              "Full Name: ${data['name']} ${data['location'].toString()}");
        }

        return Text("loading");
      },
    );
  }
}

// Widget to test the connection to db
class DojoInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CollectionReference dojos = firestore.collection('Dojo');

    return StreamBuilder<QuerySnapshot>(
      stream: dojos.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        return new ListView(
          children: snapshot.data.docs.map((DocumentSnapshot document) {
            return new ListTile(
              title: new Text(document.data()['name']),
              subtitle: new Text('[' + document.data()['location'].latitude.toString()
                  + ' , ' + document.data()['location'].longitude.toString() + ']'),
            );
          }).toList(),
        );
      },
    );
  }
}

class DojoData {

  void getMarkers(Set<Marker> markersSet)
  {
    Iterable<Marker> _markers = getInfo();
    markersSet.addAll(_markers);
  }

  Iterable<Marker> getInfo()
  {
    Set<Marker> _markers = new Set<Marker>();
    firestore.collection('Dojo').get()
        .then((QuerySnapshot querySnapshot) {
          querySnapshot.docs.forEach((dojo) {
            _markers.add(
                Marker(
                    markerId: MarkerId(dojo['name']),
                    position: LatLng(dojo['location'].latitude,
                        dojo['location'].longitude),
                    infoWindow: InfoWindow(
                        title: dojo['name'],
                        snippet: '[' + dojo['location'].latitude.toString() +
                            ' , ' + dojo['location'].longitude.toString() + ']'
                    )
                )
            );
          });
    });
    return _markers;
  }
}

/// {@template dojo}
/// Dojo model
///
/// [Dojo.empty] represents an unauthenticated dojo.
/// {@endtemplate}
class Dojo extends Equatable {
  /// {@macro dojo}
  const Dojo({
    @required this.categories,
    @required this.name,
    @required this.location,
  }) : assert(name != null);

  /// The current dojo's email address.
  final List<String> categories;

  /// The current dojo's id.
  final String name;

  /// The current dojo's name (display name).
  final List<int> location;

  /// Empty dojo which represents an unauthenticated dojo.
  static const empty = Dojo(categories: [], name: '', location: []);

  @override
  List<Object> get props => [categories, name, location];
}
