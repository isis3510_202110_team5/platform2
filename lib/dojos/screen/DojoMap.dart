import 'dart:async';
import 'dart:math';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:myapp/dojos/model/DojoDAO.dart';
import 'package:myapp/dojos/model/dojo.dart';

class DojoMap extends StatefulWidget {
  @override
  _DojoMapState createState() => _DojoMapState();
}

DojoInformation dojoinfo = new DojoInformation();

DojoData dojodata = new DojoData();

Set<Marker> _markers = Set();

CameraPosition _initialPosition = CameraPosition(
    target: LatLng(4.602363, -74.065601),
    zoom: 15
);

LatLng getClosestDojo(Map<dynamic, dynamic> packet) {
  double minDistance = double.infinity;
  LatLng refPosition = packet['startPoint'];
  LatLng closest = refPosition;
  Set<Marker> M = packet['markers'];
  double d;
  for(Marker m in M){
    if(m.markerId.value == 'Current Location') continue;
    d = sqrt(pow(refPosition.latitude - m.position.latitude, 2) +
        pow(refPosition.longitude - m.position.longitude, 2));
    if(d < minDistance){
      minDistance = d;
      closest = m.position;
    }
  }
  return closest;
}

class _DojoMapState extends State<DojoMap> {

  DataAccessObject dao = DataAccessObject();

  Completer<GoogleMapController> completer = Completer();

  Future<PermissionStatus> requestLocationPermission() async {
    final PermissionStatus permiso = await LocationPermissions()
        .checkPermissionStatus(level: LocationPermissionLevel.location);
    if (permiso != PermissionStatus.granted) {
      final PermissionStatus intento = await LocationPermissions()
          .requestPermissions(
          permissionLevel: LocationPermissionLevel.location);
      return intento;
    }
    else return permiso;
  }

  Future<void> getMarkers() async {
    List<DojoDetails> dojoList = await dao.fetchLocalDojos();
    for(DojoDetails dojo in dojoList){
      _markers.add(
        Marker(
          markerId: MarkerId(dojo.name),
          position: dojo.location,
          infoWindow: InfoWindow(
            title: dojo.name,
            snippet: '[' + dojo.location.latitude.toString() + ' , ' +
                dojo.location.longitude.toString() + ']'
          )
        )
      );
    }
  }

  Future<void> _focusCurrentLocation() async
  {
    final PermissionStatus permiso = await requestLocationPermission();
      if (permiso == PermissionStatus.granted) {
      Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
          .then((Position position) async {
        setState(() {
          _initialPosition = CameraPosition(
              target: LatLng(position.latitude, position.longitude),
              zoom: 20);
        });

        if(_markers.isEmpty) {
          _markers.add(
              Marker(
                  markerId: MarkerId('Current Location'),
                  position: LatLng(position.latitude, position.longitude)
              )
          );
        }

        final GoogleMapController controller = await completer.future;
        controller.animateCamera(CameraUpdate.newCameraPosition(_initialPosition));
      });
    }
    else{
      return Future.error('Location permissions are denied');
    }
  }

  Future<void> _focusClosestLocation() async
  {
    Geolocator geolocator = Geolocator();
    final GeolocationStatus permiso = await geolocator.checkGeolocationPermissionStatus();
    if (permiso == GeolocationStatus.granted) {
      if(_markers.length <= 1) getMarkers();
      Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
          .then((Position position) async {

        LatLng currentLoc = LatLng(position.latitude, position.longitude);
        Map packet = {'startPoint': currentLoc, 'markers': _markers};
        LatLng targetLoc = await compute(getClosestDojo, packet);
        setState(() {
          _initialPosition = CameraPosition(
              target: targetLoc,
              zoom: 20);
        });

        final GoogleMapController controller = await completer.future;
        controller.animateCamera(CameraUpdate.newCameraPosition(_initialPosition));
      });
    }
    else{
      return Future.error('Location permissions are denied');
    }
  }

  void displayBottomSheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (ctx) {
          return Container(
            height: MediaQuery
                .of(context)
                .size
                .height * 0.5,
            child: Center(
              child: dojoinfo.build(context),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Dojos Map'),
        ),
        body: Stack(
          children: <Widget>[
            GoogleMap(
              markers: _markers,
              onMapCreated: (GoogleMapController mapController) {
                completer.complete(mapController);
                _markers.addAll(dojodata.getInfo());
                //_focusCurrentLocation();
                _focusClosestLocation();
              },
              initialCameraPosition: _initialPosition,
            ),
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          child: FloatingActionButton(
          onPressed:() =>  displayBottomSheet(context),
            child: Icon(Icons.keyboard_arrow_up_sharp),
          ),
        )
    );
  }
}