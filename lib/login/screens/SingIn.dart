import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import '../../home/HomePage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class SingUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'SingUp page',
      home: Scaffold(
        appBar: AppBar(
          title: Text('SingUp page'),
        ),
        body: MyCustomForm(),
      ),
    );
  }
}

// Crea un Widget Form
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Crea una clase State correspondiente. Esta clase contendrá los datos relacionados con
// el formulario.
class MyCustomFormState extends State<MyCustomForm> {
  // Crea una clave global que identificará de manera única el widget Form
  // y nos permita validar el formulario
  //
  // Nota: Esto es un GlobalKey<FormState>, no un GlobalKey<MyCustomFormState>!
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var email;
    var marcialArt;
    var password;
    var errorPassword;
    var errorEmail;
    // Crea un widget Form usando el _formKey que creamos anteriormente
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 70.0, bottom: 40.0),
            child: Center(
              child: Container(
                  width: 200,
                  height: 150,
                  /*decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(50.0)),*/
                  child: Image.asset('assets/images/kimono__2_.png')),
            ),
          ),
          Container(
            height: 50,
            //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: TextFormField(
              obscureText: true,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Email',
                  hintText: 'Enter valid email id as abc@gmail.com'),
              validator: (value) {
                if (value.isEmpty ||
                    !value.contains('@') ||
                    !value.contains('.com')) {
                  return 'Please enter a valid email';
                } else {
                  email = value.trim();
                }
              },
            ),
          ),
          Padding(
            //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: TextFormField(
              obscureText: true,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Martial Arts',
                  hintText: 'Enter valid martial arts as taekwondo'),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter some text';
                } else {
                  marcialArt = value.trim();
                }
              },
            ),
          ),
          Padding(
            //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: TextFormField(
              obscureText: true,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Password',
                  hintText: 'Enter secure password'),
              validator: (value) {
                if (value.isEmpty ||
                    value.length < 8 ||
                    !value.contains(new RegExp(r'[0-9]')) ||
                    !value.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'))) {
                  return 'Please enter a valid password. 1 digit 1 special character and length +8';
                } else {
                  password = value.trim();
                }
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: RaisedButton(
              color: Colors.blue,
              onPressed: () async {
                // devolverá true si el formulario es válido, o falso si
                // el formulario no es válido.
                if (_formKey.currentState.validate()) {
                  print(email);
                  print(marcialArt);
                  print(password);
                  try {
                    var ans = await register(email, password);
                    if (ans == 'ok') {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (_) => HomePage()));
                    } else {
                      Toast.show(ans, context,
                          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                    }
                  } catch (e) {
                    print(e);
                  }
                }
              },
              child: Text(
                'SingUp',
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Future<String> register(String em, String p) async {
  try {
    if (em == null && p == null)
      return 'Please insert a valid email and password';
    if (em == null) return 'Please insert a valid email';
    if (p == null) return 'Please insert a valid email';

    if (em.isEmpty) {
      return 'Please insert email';
    }
    if (!em.contains('@') || !em.contains('.com')) {
      return 'Please insert a valid email';
    }
    if (p.isEmpty) {
      return 'Please insert a password';
    }
    if (p.length < 8 ||
        !p.contains(new RegExp(r'[0-9]')) ||
        !p.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'))) {
      return 'Please insert a valid password';
    }
    UserCredential userCredential = await FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: em, password: p);
    print(userCredential);
    return 'ok';
  } on FirebaseAuthException catch (e) {
    if (e.code == 'weak-password') {
      return 'The password provided is too weak.';
    } else if (e.code == 'email-already-in-use') {
      return 'The account already exists for that email.';
    }
  } catch (e) {
    return e.toString();
  }
}
