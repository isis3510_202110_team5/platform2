// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:myapp/login/screens/SingIn.dart';
import 'package:toast/toast.dart';
import '../../home/HomePage.dart';
import 'package:firebase_auth/firebase_auth.dart';

FirebaseAuth auth = FirebaseAuth.instance;

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginDemo(),
    );
  }
}

class LoginDemo extends StatefulWidget {
  @override
  _LoginDemoState createState() => _LoginDemoState();
}

class _LoginDemoState extends State<LoginDemo> {
  String emailurl;
  String pssword;
  String errorEmail = '';
  String errorPassword = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("LogIn Page"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 70.0, bottom: 40.0),
              child: Center(
                child: Container(
                    width: 200,
                    height: 150,
                    /*decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(50.0)),*/
                    child: Image.asset('assets/images/fight.png')),
              ),
            ),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                    hintText: 'Enter valid email id as abc@gmail.com'),
                validator: (value) {
                  if (value.isEmpty ||
                      !value.contains('@') ||
                      !value.contains('.com')) {
                    errorEmail = 'Please enter a valid email';
                    return 'Please enter a valid email';
                  } else {
                    errorEmail = '';
                    emailurl = value.trim();
                  }
                },
              ),
            ),
            Container(
                child: Text(
              errorEmail,
              style: TextStyle(color: Colors.red),
            )),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                    hintText: 'Enter secure password'),
                validator: (value) {
                  if (value.isEmpty ||
                      value.length < 8 ||
                      !value.contains(new RegExp(r'[0-9]')) ||
                      !value.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'))) {
                    errorEmail =
                        'Please enter a valid password. 1 digit 1 special character and length +8';
                    return 'Please enter a valid password. 1 digit 1 special character and length +8';
                  } else {
                    errorEmail = '';
                    pssword = value.trim();
                  }
                },
              ),
            ),
            Container(
                child: Text(
              errorPassword,
              style: TextStyle(color: Colors.red),
            )),
            FlatButton(
              onPressed: () {
                //TODO FORGOT PASSWORD SCREEN GOES HERE
              },
              child: Text(
                'Forgot Password',
                style: TextStyle(color: Colors.blue, fontSize: 15),
              ),
            ),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(20)),
              child: FlatButton(
                onPressed: () async {
                  try {
                    var ans =
                        await signInWithEmailAndPassword(emailurl, pssword);
                    print(ans);
                    if (ans == 'ok') {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (_) => HomePage()));
                    } else {
                      Toast.show(ans, context,
                          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                    }
                  } catch (e) {
                    print(e);
                  }
                },
                child: Text(
                  'LogIn',
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            SizedBox(
              height: 130,
            ),
            FlatButton(
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => SingUp()));
              },
              child: Text(
                'New User? Create Account',
                style: TextStyle(color: Colors.blue, fontSize: 15),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Future<String> signInWithEmailAndPassword(email, password) async {
  try {
    print(email);
    print(password);
    if (email == null && password == null) {
      return 'Please insert a valid email and password';
    }
    if (email == null || !email.contains('@') || !email.contains('.com')) {
      return 'Please insert a valid email';
    }

    if (password == null ||
        password.length < 8 ||
        !password.contains(new RegExp(r'[0-9]')) ||
        !password.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'))) {
      return 'Please insert a valid password';
    }
    if (email.isEmpty) {
      return 'Please insert email';
    }
    if (password.isEmpty) {
      return 'Please insert a password';
    }
    UserCredential userCredential = await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password);
    print(userCredential);

    return 'ok';
  } on FirebaseAuthException catch (e) {
    if (e.code == 'user-not-found') {
      return 'No user found for that email.';
    } else if (e.code == 'wrong-password') {
      return 'Wrong password provided for that user.';
    }
  }
}

Future<String> register() async {
  try {
    await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: "barry.allen@example.com", password: "SuperSecretPassword!");
    print('ok');
    return 'ok';
  } on FirebaseAuthException catch (e) {
    if (e.code == 'weak-password') {
      return 'The password provided is too weak.';
    } else if (e.code == 'email-already-in-use') {
      return 'The account already exists for that email.';
    }
  } catch (e) {
    return e.toString();
  }
}
