import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:geolocator/geolocator.dart';
import 'package:myapp/channels/screen/channels.dart';
import 'package:myapp/competitions/screen/CompetitionsPage.dart';
import 'package:myapp/dojos/screen/DojoMap.dart';
import 'package:myapp/home/model/home.dart';
import 'package:myapp/learn_more/view/learMoreHome.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class CustomCacheManager {
  static const key = 'customCacheKey';
  static CacheManager instance = CacheManager(
    Config(
      key,
      stalePeriod: const Duration(days: 7),
      maxNrOfCacheObjects: 20,
      repo: JsonCacheInfoRepository(databaseName: key),
      //fileSystem: IOFileSystem(key),
      fileService: HttpFileService(),
    ),
  );
}

class _HomePageState extends State<HomePage> {
  Future<void> verifyConnection(BuildContext context) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      if (await Geolocator().isLocationServiceEnabled())
        Navigator.push(context, MaterialPageRoute(builder: (_) => DojoMap()));
      else {
        return showDialog(
            context: context,
            builder: (context) {
              return releaseDialog(
                  context,
                  "GPS Needed",
                  "The GPS is recquired "
                      "for Dojo Map to work. Please activate it and try again");
            });
      }
    } else {
      return showDialog(
          context: context,
          builder: (context) {
            return releaseDialog(
                context,
                "Connectivity Needed",
                "The Dojos Map could not be "
                    "loaded because the connection is lost. Please review and try again");
          });
    }
  }

  SimpleDialog releaseDialog(
      BuildContext context, String title, String message) {
    return SimpleDialog(
      title: Text(title),
      children: <Widget>[
        Center(child: Text(message)),
        Center(
            child: TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                }))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Page'),
      ),
      body: ListView(
        children: [
          Card(
              clipBehavior: Clip.antiAlias,
              margin: const EdgeInsets.only(
                  top: 20.0, bottom: 10.0, left: 10.0, right: 15.0),
              child: new InkWell(
                  onTap: () {
                    Navigator.push(
                        context, MaterialPageRoute(builder: (_) => Channels()));
                  },
                  child: Container(
                    decoration: BoxDecoration(color: Colors.lightBlueAccent),
                    child: Column(
                      children: [
                        Padding(padding: const EdgeInsets.all(10.0)),
                        ListTile(
                          leading: CircleAvatar(
                            child: Image.asset('assets/images/charla.png'),
                            radius: 50,
                            backgroundColor: Colors.white,
                          ),
                          title: const Text(
                            'Channels',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 17.0),
                          ),
                          subtitle: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 2,
                                child: Text(
                                  'More than 10 channels available. Most popular today: kunfupanda',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.normal),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(padding: const EdgeInsets.all(10.0)),
                        /*Container(height: 200, child: HomeInformation()),*/
                        Row(children: <Widget>[
                          Padding(padding: const EdgeInsets.all(10.0)),
                          const Icon(Icons.favorite,
                              size: 16.0, color: Colors.white),
                          Padding(padding: const EdgeInsets.all(2.0)),
                          Text(
                            '100 followers',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.normal),
                          ),
                          Padding(padding: const EdgeInsets.all(5.0)),
                          const Icon(Icons.email,
                              size: 16.0, color: Colors.white),
                          Padding(padding: const EdgeInsets.all(2.0)),
                          Text(
                            '200 messages',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.normal),
                          ),
                        ]),
                        Padding(padding: const EdgeInsets.all(10.0)),
                      ],
                    ),
                  ))),
          Card(
            margin: const EdgeInsets.only(
                top: 10.0, bottom: 10.0, left: 10.0, right: 15.0),
            clipBehavior: Clip.antiAlias,
            child: new InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (_) => CompetitionsPage()));
              },
              child: Container(
                decoration: BoxDecoration(color: Colors.lightBlueAccent),
                child: Column(
                  children: [
                    Padding(padding: const EdgeInsets.all(10.0)),
                    ListTile(
                      leading: CircleAvatar(
                        child: Image.asset('assets/images/pagoda__1_.png'),
                        radius: 50,
                        backgroundColor: Colors.white,
                      ),
                      title: const Text(
                        'Competitions',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 17.0),
                      ),
                      subtitle: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: Text(
                              'Subscribe to a competitio and see results. Most popular today: Jishin Karate JKA',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.normal),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(padding: const EdgeInsets.all(10.0)),
                    Row(children: <Widget>[
                      Padding(padding: const EdgeInsets.all(10.0)),
                      const Icon(Icons.favorite,
                          size: 16.0, color: Colors.white),
                      Padding(padding: const EdgeInsets.all(2.0)),
                      Text(
                        '100 followers',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.normal),
                      ),
                      Padding(padding: const EdgeInsets.all(5.0)),
                      const Icon(Icons.email, size: 16.0, color: Colors.white),
                      Padding(padding: const EdgeInsets.all(2.0)),
                      Text(
                        '200 messages',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.normal),
                      ),
                    ]),
                    Padding(padding: const EdgeInsets.all(10.0)),
                  ],
                ),
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.only(
                top: 10.0, bottom: 10.0, left: 10.0, right: 15.0),
            clipBehavior: Clip.antiAlias,
            child: new InkWell(
              onTap: () {
                verifyConnection(context);
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => DojoMap()));
              },
              child: Container(
                decoration: BoxDecoration(color: Colors.lightBlueAccent),
                child: Column(
                  children: [
                    Padding(padding: const EdgeInsets.all(10.0)),
                    ListTile(
                      leading: CircleAvatar(
                        child: Image.asset('assets/images/location__1_.png'),
                        radius: 50,
                        backgroundColor: Colors.white,
                      ),
                      title: const Text(
                        'Nearby Dojos',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 17.0),
                      ),
                      subtitle: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: Text(
                              '5 dojos near your location',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.normal),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(padding: const EdgeInsets.all(10.0)),
                    Row(children: <Widget>[
                      Padding(padding: const EdgeInsets.all(10.0)),
                      const Icon(Icons.favorite,
                          size: 16.0, color: Colors.white),
                      Padding(padding: const EdgeInsets.all(2.0)),
                      Text(
                        '100 followers',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.normal),
                      ),
                      Padding(padding: const EdgeInsets.all(5.0)),
                      const Icon(Icons.email, size: 16.0, color: Colors.white),
                      Padding(padding: const EdgeInsets.all(2.0)),
                      Text(
                        '200 messages',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.normal),
                      ),
                    ]),
                    Padding(padding: const EdgeInsets.all(10.0)),
                  ],
                ),
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.only(
                top: 10.0, bottom: 10.0, left: 10.0, right: 15.0),
            clipBehavior: Clip.antiAlias,
            child: new InkWell(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => LearMoreHome()));
              },
              child: Container(
                decoration: BoxDecoration(color: Colors.lightBlueAccent),
                child: Column(
                  children: [
                    Padding(padding: const EdgeInsets.all(10.0)),
                    ListTile(
                      leading: CircleAvatar(
                        child: Image.asset('assets/images/book.png'),
                        radius: 50,
                        backgroundColor: Colors.white,
                      ),
                      title: const Text(
                        'Learn More',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 17.0),
                      ),
                      subtitle: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: Text(
                              'Our best teachers show routines and diets to improve your skills. Most popular today: Daily Routine',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.normal),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(padding: const EdgeInsets.all(10.0)),
                    Row(children: <Widget>[
                      Padding(padding: const EdgeInsets.all(10.0)),
                      const Icon(Icons.favorite,
                          size: 16.0, color: Colors.white),
                      Padding(padding: const EdgeInsets.all(2.0)),
                      Text(
                        '100 followers',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.normal),
                      ),
                      Padding(padding: const EdgeInsets.all(5.0)),
                      const Icon(Icons.email, size: 16.0, color: Colors.white),
                      Padding(padding: const EdgeInsets.all(2.0)),
                      Text(
                        '200 messages',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.normal),
                      ),
                    ]),
                    Padding(padding: const EdgeInsets.all(10.0)),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
