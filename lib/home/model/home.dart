import 'package:cached_network_image/cached_network_image.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

FirebaseFirestore firestore = FirebaseFirestore.instance;

class AddCompetency extends StatelessWidget {
  final String name;
  final String description;
  final String coverImg;
  final int followers;
  final String publisherId;

  AddCompetency(this.name, this.description, this.coverImg, this.followers,
      this.publisherId);

  @override
  Widget build(BuildContext context) {
    CollectionReference competencys =
        FirebaseFirestore.instance.collection('Competency');

    Future<void> addCompetency() {
      return competencys
          .add({
            'name': name,
            'description': description,
            'coverImg': coverImg,
            'followers': followers,
            'publisherId': publisherId
          })
          .then((value) => print("Competency Added"))
          .catchError((error) => print("Failed to add competency: $error"));
    }

    return TextButton(
      onPressed: addCompetency,
      child: Text(
        "Add Competency",
      ),
    );
  }
}

class GetCompetencyName extends StatelessWidget {
  final String documentId;

  GetCompetencyName(this.documentId);

  @override
  Widget build(BuildContext context) {
    CollectionReference competencys =
        FirebaseFirestore.instance.collection('Competency');

    return FutureBuilder<DocumentSnapshot>(
      future: competencys.doc(documentId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Center(
              child: Text(
                  "Something went wrong. Please check your internet connection and try again."));
        }

        if (snapshot.hasData && !snapshot.data.exists) {
          return Text("Document does not exist");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data();
          return Card(
            clipBehavior: Clip.antiAlias,
            child: Container(
              height: 200.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage("${data['coverImg']}"),
                  colorFilter:
                      ColorFilter.mode(Colors.grey[600], BlendMode.modulate),
                  fit: BoxFit.fitWidth,
                  alignment: Alignment.topCenter,
                ),
              ),
              child: Column(
                children: [
                  Padding(padding: const EdgeInsets.all(40.0)),
                  ListTile(
                    leading: CachedNetworkImage(
                      imageUrl: "${data['coverImg']}",
                      imageBuilder: (context, imageProvider) => Container(
                        width: 50.0,
                        height: 50.0,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: imageProvider, fit: BoxFit.cover),
                        ),
                      ),
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) =>
                          Center(child: Icon(Icons.error)),
                    ),
                    title: Text(
                      "${data['name']}",
                      style: TextStyle(
                          color: Colors.white24,
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0),
                    ),
                    subtitle: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Text(
                            "${data['description']}",
                            style: TextStyle(
                                color: Colors.white.withOpacity(0.6),
                                fontWeight: FontWeight.normal),
                          ),
                        ),
                        const Icon(Icons.favorite,
                            size: 16.0, color: Colors.white),
                        Padding(padding: const EdgeInsets.all(5.0)),
                        const Icon(Icons.remove_red_eye,
                            size: 16.0, color: Colors.white),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        }

        return CircularProgressIndicator();
      },
    );
  }
}

// Widget to test the connection to db
class HomeInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CollectionReference competencys = firestore.collection('Competency');

    return StreamBuilder<QuerySnapshot>(
      stream: competencys.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        return new ListView(
          children: snapshot.data.docs.map((DocumentSnapshot document) {
            print(document.data());
            return new ListTile(
              title: new Text(document.data()['followers']),
            );
          }).toList(),
        );
      },
    );
  }
}

/// {@template competency}
/// Competency model
///
/// [Competency.empty] represents an unauthenticated competency.
/// {@endtemplate}
class Competency extends Equatable {
  /// {@macro competency}
  const Competency({
    @required this.name,
    @required this.description,
    @required this.coverImg,
    @required this.followers,
    @required this.publisherId,
  }) : assert(name != null);

  /// The current competency's id.
  final String name;
  final String description;
  final String coverImg;
  final int followers;
  final String publisherId;

  /// Empty competency which represents an unauthenticated competency.
  static const empty = Competency(
      name: '', description: '', coverImg: '', followers: 0, publisherId: '');

  @override
  List<Object> get props =>
      [name, description, coverImg, followers, publisherId];
}
