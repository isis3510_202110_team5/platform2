import 'package:flutter/material.dart';
import 'package:myapp/competitions/model/competency.dart';
import 'package:myapp/competitions/services/competitions_service.dart';

import 'package:myapp/users/screens/User.dart';

class CompetitionsPage extends StatefulWidget {
  final List<String> list = List.generate(10, (index) => "Text $index");
  @override
  _CompetitionsPageState createState() => _CompetitionsPageState();
}

class Search extends SearchDelegate {
  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.close),
        onPressed: () {
          query = "";
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  String selectedResult;

  @override
  Widget buildResults(BuildContext context) {
    return Container(
      child: Center(
        child: Text(selectedResult),
      ),
    );
  }

  final List<String> listReferent;
  Search(this.listReferent);
  List<String> recentList = ["Zero", "One", "Two"];

  @override
  Widget buildSuggestions(BuildContext context) {
    List<String> suggestedList = [];
    query.isEmpty
        ? suggestedList = ["Zero", "One", "Two"]
        : suggestedList.addAll(listReferent.where(
            (element) => element.contains(query),
          ));

    return ListView.builder(
      itemCount: suggestedList.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(
            suggestedList[index],
          ),
          leading: query.isEmpty ? Icon(Icons.access_time) : SizedBox(),
          onTap: () {
            selectedResult = suggestedList[index];
            showResults(context);
          },
        );
      },
    );
  }
}

class _CompetitionsPageState extends State<CompetitionsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Competition'),
      ),
      body: ListView(children: [
        Text(connection.toString() + ''),
        GetCompetencyName('996MEMxecnMmYSRNlfY5'),
        GetCompetencyName('dt1vD76zXxHMUH5i3zcM'),
        GetCompetencyName('7QFw9QRm5Zyzp8Mco98V'),
        GetCompetencyName('fdGZj4pQR6DKNWNsQpBV'),
      ]),
    );
  }
}
