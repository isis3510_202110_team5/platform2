import 'package:connectivity/connectivity.dart';

var subscription;
var connection;

@override
initState() {
  subscription =
      Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
    print(result);
    connection = result;
    // Got a new connectivity status!
  });
}

// Be sure to cancel subscription after you are done
@override
dispose() {
  subscription.cancel();
}
