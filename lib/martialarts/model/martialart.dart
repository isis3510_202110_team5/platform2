import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

FirebaseFirestore firestore = FirebaseFirestore.instance;

class AddMartialArt extends StatelessWidget {
  final String name;

  AddMartialArt(this.name);

  @override
  Widget build(BuildContext context) {
    // Create a CollectionReference called martialArts that references the firestore collection
    CollectionReference martialArts =
        FirebaseFirestore.instance.collection('MartialArt');

    Future<void> addMartialArt() {
      // Call the martialArt's CollectionReference to add a new martialArt
      return martialArts
          .add({
            'name': name, // John Doe
          })
          .then((value) => print("MartialArt Added"))
          .catchError((error) => print("Failed to add martialArt: $error"));
    }

    return TextButton(
      onPressed: addMartialArt,
      child: Text(
        "Add MartialArt",
      ),
    );
  }
}

class GetMartialArtName extends StatelessWidget {
  final String documentId;

  GetMartialArtName(this.documentId);

  @override
  Widget build(BuildContext context) {
    CollectionReference martialArts =
        FirebaseFirestore.instance.collection('MartialArt');

    return FutureBuilder<DocumentSnapshot>(
      future: martialArts.doc(documentId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.hasData && !snapshot.data.exists) {
          return Text("Document does not exist");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data();
          return Text("Full Name: ${data['name']}");
        }

        return Text("loading");
      },
    );
  }
}

// Widget to test the connection to db
class MartialArtInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CollectionReference martialArts = firestore.collection('MartialArt');

    return StreamBuilder<QuerySnapshot>(
      stream: martialArts.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        return new ListView(
          children: snapshot.data.docs.map((DocumentSnapshot document) {
            return new ListTile(
              title: new Text(document.data()['name']),
            );
          }).toList(),
        );
      },
    );
  }
}

/// {@template martialArt}
/// MartialArt model
///
/// [MartialArt.empty] represents an unauthenticated martialArt.
/// {@endtemplate}
class MartialArt extends Equatable {
  /// {@macro martialArt}
  const MartialArt({
    @required this.name,
  }) : assert(name != null);

  /// The current martialArt's id.
  final String name;

  /// Empty martialArt which represents an unauthenticated martialArt.
  static const empty = MartialArt(name: '');

  @override
  List<Object> get props => [name];
}
