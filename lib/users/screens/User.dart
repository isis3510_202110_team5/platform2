import 'package:flutter/material.dart';

class User extends StatelessWidget {
  final String user;

  const User({Key key, this.user}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Image(
          height: 75, width: 75, image: AssetImage('assets/images/fight.png')),
      Text('Valentina escudero',
          style: TextStyle(color: Colors.black, fontSize: 30)),
      Text('Nivel intermedio de Judo y básico de Taekwondo',
          style: TextStyle(color: Colors.black45, fontSize: 15))
    ]);
  }
}
