import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

FirebaseFirestore firestore = FirebaseFirestore.instance;

class AddUser extends StatelessWidget {
  final String martialArt;
  final int age;
  final String name;

  AddUser(this.martialArt, this.age, this.name);

  @override
  Widget build(BuildContext context) {
    // Create a CollectionReference called users that references the firestore collection
    CollectionReference users = FirebaseFirestore.instance.collection('User');

    Future<void> addUser() {
      // Call the user's CollectionReference to add a new user
      return users
          .add({
            'MartialArts': martialArt, // John Doe
            'age': age, // Stokes and Sons
            'name': name
          })
          .then((value) => print("User Added"))
          .catchError((error) => print("Failed to add user: $error"));
    }

    return TextButton(
      onPressed: addUser,
      child: Text(
        "Add User",
      ),
    );
  }
}

class GetUserName extends StatelessWidget {
  final String documentId;

  GetUserName(this.documentId);

  @override
  Widget build(BuildContext context) {
    CollectionReference users = FirebaseFirestore.instance.collection('User');

    return FutureBuilder<DocumentSnapshot>(
      future: users.doc(documentId).get(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.hasData && !snapshot.data.exists) {
          return Text("Document does not exist");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          Map<String, dynamic> data = snapshot.data.data();
          return Text("Full Name: ${data['name']} ${data['age']}");
        }

        return Text("loading");
      },
    );
  }
}

// Widget to test the connection to db
class UserInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CollectionReference users = firestore.collection('User');

    return StreamBuilder<QuerySnapshot>(
      stream: users.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        return new ListView(
          children: snapshot.data.docs.map((DocumentSnapshot document) {
            return new ListTile(
              title: new Text(document.data()['name']),
              subtitle: new Text(document.data()['age'].toString()),
            );
          }).toList(),
        );
      },
    );
  }
}

/// {@template user}
/// User model
///
/// [User.empty] represents an unauthenticated user.
/// {@endtemplate}
class User extends Equatable {
  /// {@macro user}
  const User({
    @required this.martialArts,
    @required this.age,
    @required this.name,
  })  : assert(name != null),
        assert(martialArts != null);

  /// The current user's email address.
  final String martialArts;

  /// The current user's id.
  final String name;

  /// The current user's name (display name).
  final int age;

  /// Empty user which represents an unauthenticated user.
  static const empty = User(martialArts: '', name: '', age: 0);

  @override
  List<Object> get props => [martialArts, name, age];
}
